## Masterclass Test-Driven Development exercise material

Exercise material for Avisi's masterclass on Test-Driven Development. The masterclass consists of two exercices:
[Leap Years](https://sammancoaching.org/kata_descriptions/leap_years.html) and
[Shopping Basket](https://sammancoaching.org/kata_descriptions/shopping_basket.html).

### Branches and MR's

* The `main` branch contains some skeleton code, which provides a starting point for the exercises.
* The `leap-years` and `shopping-basket` branches contain the "answers" to the exercises. Every commit in these branches
  represents a single small step. To make it easier to walk through those steps, two merge requests have been created.
  Below are links to the first commit of each MR. The "Next" and "Previous" buttons can be used to easily browse through
  the commits.
  * [Leap Years](https://gitlab.com/avisi/avisi_academy/test-driven-development-masterclass/-/merge_requests/1/diffs?commit_id=4691798de7895f299f4dd93dbf618beae410ce0d)
  * [Shopping Basket](https://gitlab.com/avisi/avisi_academy/test-driven-development-masterclass/-/merge_requests/2/diffs?commit_id=681c607a200a4379ef56d05f008d54b357fd0196)

## Attribution

The Leap Years and Shopping Basket exercises are provided by Emily Bache of https://sammancoaching.org, under
the [Creative Commons Attribution-ShareAlike 4.0 license](https://sammancoaching.org/LICENSE.html).
